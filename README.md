MY VIM SETTINGS
===============

Hi everyone! This is my vim settings file (.vimrc) and i wanna share it with you.  
Greetings!

Install
-------

I use [Vundle](https://github.com/VundleVim/Vundle.vim) to install plugins and [Solarize](https://github.com/altercation/vim-colors-solarized) for the style.
If you want to install it, must follow these steps:

~~~
# Go to home directory
~$ cd ~/

# Verify if doesn't exist _~/.vim dir_ and clone the repo
~$ git clone https://gitlab.com/fdannmon/my-vim-settings.git ~/.vim

# Create a symbolic link between your _~/.vimrc_ and this _vimrc_ file
~$ ln -s .vim/vimrc ~/.vimrc
~~~

